import React, {Component} from 'react';
import InputFile from './components/InputFile';
import {Toggler, TogglerItem} from './components/Toggler';
import Input from './components/Input';

import './App.scss';

class App extends Component {

    state = {
        file: './img.png',
        test: 'test_1',
        test2: 'test_2',
        value: ''
    };


    changeToggler = (name, value) => (e) => {
        this.setState({
            [name]: value
        })
    };

    setNewImg = (file) => {
        this.setState({file});
    };

    setInputValue = (even) => {
        this.setState({value: even.target.value});
    };

    render() {
        const {setNewImg, changeToggler, setInputValue} = this;
        const {file, test, value} = this.state;

        return (
            <div className="App">
                <InputFile previewImg={file} handler={setNewImg}/>
                <Input name="Name"
                       type="text"
                       placeholder="First name"
                       value={value}
                       handler={setInputValue}
                       contentMaxLength={20}
                />
                <Toggler
                    label={test}
                    value={test}
                    changeStatus={changeToggler}
                    name="test">

                    <TogglerItem value="test_1"/>
                    <TogglerItem value="test_2"/>
                    <TogglerItem value="test_3"/>
                    <TogglerItem value="test_4"/>
                </Toggler>
            </div>
        )
    }
}

export default App;
