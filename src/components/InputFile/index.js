import React from "react";
import './style.scss';
import PropTypes from "prop-types";
import Input from "../Input";

const InputFile = ({previewImg, handler}) => {
    const setPreview = (even) => {
        const file = even.target.files[0];
        const reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
            reader.addEventListener('load', () => {
                handler(reader.result);
            });
        }
    };

    return (
        <div className="input-file">
            <label className="input-file__label">
                <input type="file" className="input-file__value" onChange={setPreview}/>
                <img className="input-file__img" src={previewImg} alt=""/>
            </label>
        </div>
    )
};


InputFile.propTypes = {
    previewImg: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired
};

InputFile.defaultProps = {
    previewImg: './no-image.png'
};

export default InputFile;