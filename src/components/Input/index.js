import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Input = ({name, type, placeholder, value, handler, contentLength, contentMaxLength}) => {
    return (
        <label className="input">
            <span className="input__title">{name}</span>
            {(contentLength) ? <span className="input__count">{value.length}</span> : null}
            <input
                className="input__value"
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={handler}
                maxLength={contentMaxLength}
            />
        </label>
    );
};


Input.propTypes = {
    type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number
};

Input.defaultProps = {
    type: 'text',
    placeholder: 'Text...',
    contentMaxLength: 50
};


export default Input;

